﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace sorter_cmd {
    public class rule_storage_xml : rule_storage {

        /// <summary>
        /// Commits rule objects to xml file
        /// </summary>
        public override void save_rules (string ruleset_location, List<rule> rules) {

            XDocument xdoc = new XDocument(new XElement("rules",
                from rule in rules
                select new XElement("rule",
                    new XElement("pattern", rule.Pattern),
                    new XElement("source_directory", rule.SourceDirectory),
                    new XElement("destination_directory", rule.DestinationDirectory),
                    new XElement("regex", rule.Regex),
                    new XElement("action", rule.Action.GetHashCode())
                    )
                )
            );

            xdoc.Save(ruleset_location);
        }

        /// <summary>
        /// Given a ruleset file location, parses rules from storage
        /// returns a List&lt;rule&gt; of rules
        /// <param name="ruleset_location">String with ruleset file location on storage</param>
        /// <returns>List&lt;rule&gt; of rules</returns>
        /// </summary>
        public override List<rule> load_rules (string ruleset_location) {

            var rules = new List<rule>();

            // Simple check for now. Has to be more rigorous in the future.
            if ( !File.Exists(ruleset_location) ) {
                XmlWriter xmlrules = XmlWriter.Create(ruleset_location);
                return rules;
            }

            // Try loading the ruleset to check XML validity
            XDocument xmlruleset = new XDocument();
            try {
                xmlruleset = XDocument.Load(ruleset_location);
                Console.WriteLine("Loaded existing ruleset");
            }
            catch ( XmlException exception ) {
                Console.WriteLine("Bad XML rulset");
                Console.WriteLine(exception.Data);
            }

            rules =
                ( from r in xmlruleset.Descendants("rule")
                  select new rule() {
                      Pattern = r.Element("pattern").Value,
                      SourceDirectory = r.Element("source_directory").Value,
                      DestinationDirectory = r.Element("destination_directory").Value,
                      Regex = bool.Parse(r.Element("regex").Value),
                      Action = (rule.Actions)int.Parse(r.Element("action").Value),
                  } ).ToList();

            return rules;
        }

    }
}
