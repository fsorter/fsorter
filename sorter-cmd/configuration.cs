﻿using System;
using CommandLine;

namespace sorter_cmd {

    /// <summary>
    /// In progress: application configuration class
    /// Do you move the XML load/store methods from srt_helper?
    /// No, create a generic persistent configuration interface and write an XML implementation
    /// Note: these are ephemeral settings only. these can only be set when running from command line.
    /// At this point (sorter-cmd 0.5.8/sorter-gui 0.3.0), the command line flags do not affect GUIs.
    /// </summary>
    /// 
    class Configuration {

        // Parsed options

        /// <summary>
        /// Print version and exit flag
        /// </summary>
        [Option('v', "version", DefaultValue = false, Required = false,
            HelpText = "Prints version number")]
        public Boolean version { get; set; }

        /// <summary>
        /// Run the user rules without interactive prompt flag
        /// </summary>
        [Option('r', "run", DefaultValue = false, Required = false,
            HelpText = "Runs ruleset in user profile")]
        public Boolean runrules { get; set; }

        /// <summary>
        /// Folder cleanup without prompt after run flag
        /// </summary>
        [Option('q', "quiet-cleanup", DefaultValue = false, Required = false,
            HelpText = "Don't prompt on cleanup")]
        public Boolean quietcleanup { get; set; }

        /// <summary>
        /// Do not run folder cleanup after run flag
        /// </summary>
        [Option('n',"no-cleanup", DefaultValue = false, Required = false,
            HelpText = "Don't cleanup after run")]
        public Boolean nocleanup { get; set; }

        /// <summary>
        /// Do not run delete rules flag
        /// </summary>
        [Option('s', "no-delete", DefaultValue = false, Required = false,
            HelpText = "Don't run delete rules")]
        public Boolean nodelete { get; set; }

        /// <summary>
        /// Run move rules as copy rules flag
        /// </summary>
        [Option("move-as-copy", DefaultValue = false, Required = false,
            HelpText = "Run move rules as copy rules")]
        public Boolean moveascopy { get; set; }

        public Boolean isValid;

        /// <summary>
        /// Initialized by main.
        /// What is this platform?
        /// </summary>
        public Boolean isLinux { get; set; } = false;

        // Configuration options
        public System.Version app_version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

        public Configuration () {

        }

        /// <summary>
        /// Takes in the args from command line, parses and validates the flags.
        /// </summary>
        /// <param name="args">Command line arguments/flags</param>
        public Configuration (string[] args) {

            // Consider: in the event that the flags aren't parsable, terminate or continue?
            isValid = CommandLine.Parser.Default.ParseArgumentsStrict(args, this);

            // Short circuit the whole application run if version flag is set
            if ( true == this.version ) {
                System.Console.WriteLine("sorter-cmd version " + this.app_version);
                Environment.Exit(0);
            }

            // Check that non-interactive flags are predicate by a non-interactive run
            if ( false == this.runrules ) {
                if ( true == this.nocleanup ||
                     true == this.nodelete ||
                     true == this.quietcleanup
                    ) {
                    Console.WriteLine("Non-interactive flag set without -r");
                    Environment.Exit(1);
                }
            }

            // Check that nocleanup and quietcleanup are not both set
            if ( true == this.nocleanup && true == this.quietcleanup ) {
                Console.WriteLine("Error: both no-delete and quiet-cleanup set");
                Environment.Exit(1);
            }
        }

    }
}
