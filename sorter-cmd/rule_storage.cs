﻿using System.Collections.Generic;

namespace sorter_cmd {
    public class rule_storage {

        public virtual void save_rules (string ruleset_location, List<rule> rules) {
            return;
        }


        public virtual List<rule> load_rules (string ruleset_location) {
            return new List<rule>();
        }

    }
}
