﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;
using System.Text.RegularExpressions;

namespace sorter_cmd {
    
    /// <summary>
    /// Contains helper functions for the sorter program
    /// </summary>
    public class srt_helper {
        [Flags]
        public enum EnumWalk : short {
            Dir = 0x01,
            File = 0x02,
        }

        /// <summary>
        /// Gets a folder path from user using Microsoft.WindowsAPICodePack.Dialogs
        /// </summary>
        /// <param name="Multiselect">Specify whether user can select multiple folders</param>
        /// <returns>String array of folder names</returns>
        public string[] FolderPicker (bool Multiselect) {
            var cfd = new CommonOpenFileDialog();
            cfd.IsFolderPicker = true;
            cfd.Multiselect = Multiselect;

            try {
                cfd.ShowDialog();
                return cfd.FileNames.ToArray();
            }
            catch {
                // Program can end up here in one of two ways:
                // 1. User hits cancel on the dialog
                // 2. Something I have no clue about happens
                Debug.WriteLine("Something unexpected happened in the folder picker");
                return new string[] {""};
            }
        }

        /// <summary>
        /// Gets contents of a directory
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumberable&lt;string&gt;
        /// </returns>
        public System.Collections.Generic.IEnumerable<string> ls (string Location, EnumWalk DirOrFile = EnumWalk.Dir) {
            try {
                switch ( DirOrFile ) {
                    case EnumWalk.Dir:
                        return System.IO.Directory.EnumerateDirectories(Location);
                    case EnumWalk.File:
                        return System.IO.Directory.EnumerateFiles(Location);
                    default:
                        break;
                }
            }
            catch {
                Console.WriteLine("files' on fire yo");
                throw;
            }

            return null;
        }

        public void lsout (System.Collections.Generic.IEnumerable<string> ToPrint) {
            foreach ( var p in ToPrint ) {
                Console.WriteLine(p);
            }
        }

        public void lsout (System.Collections.Generic.IEnumerable<System.IO.FileInfo> ToPrint) {
            foreach ( var p in ToPrint ) {
                Console.WriteLine(p.Name);
            }
        }

        /// <summary>
        /// Returns a list of FileInfo objects under a given root
        /// Lifted from https://msdn.microsoft.com/en-us/library/bb513869.aspx
        /// TODO: Look into optimizing this thing's memory usage
        /// </summary>
        public List<System.IO.FileInfo> ls2 (string root) {
            // Data structure to hold names of subfolders to be
            // examined for files.
            Stack<string> dirs = new Stack<string>(20);
            var resfiles = new List<System.IO.FileInfo>();


            if ( !System.IO.Directory.Exists(root) ) {
                throw new ArgumentException();
            }
            dirs.Push(root);

            while ( dirs.Count > 0 ) {
                string currentDir = dirs.Pop();
                string[] subDirs;
                try {
                    subDirs = System.IO.Directory.GetDirectories(currentDir);
                }
                // An UnauthorizedAccessException exception will be thrown if we do not have
                // discovery permission on a folder or file. It may or may not be acceptable 
                // to ignore the exception and continue enumerating the remaining files and 
                // folders. It is also possible (but unlikely) that a DirectoryNotFound exception 
                // will be raised. This will happen if currentDir has been deleted by
                // another application or thread after our call to Directory.Exists. The 
                // choice of which exceptions to catch depends entirely on the specific task 
                // you are intending to perform and also on how much you know with certainty 
                // about the systems on which this code will run.
                catch ( UnauthorizedAccessException e ) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                catch ( System.IO.DirectoryNotFoundException e ) {
                    Console.WriteLine(e.Message);
                    continue;
                }

                string[] files = null;
                try {
                    files = System.IO.Directory.GetFiles(currentDir);
                }

                catch ( UnauthorizedAccessException e ) {

                    Console.WriteLine(e.Message);
                    continue;
                }

                catch ( System.IO.DirectoryNotFoundException e ) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                // Perform the required action on each file here.
                // Modify this block to perform your required task.
                foreach ( string file in files ) {
                    try {
                        // Perform whatever action is required in your scenario.
                        // var fi = new System.IO.FileInfo(file);
                        resfiles.Add(new System.IO.FileInfo(file));
                        //Console.WriteLine("{0}: {1}, {2}", fi.Name, fi.Length, fi.CreationTime);
                    }
                    catch ( System.IO.FileNotFoundException e ) {
                        // If file was deleted by a separate application
                        // or thread since the call to ls2()
                        // then just continue.
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }

                // Push the subdirectories onto the stack for traversal.
                // This could also be done before handing the files.
                foreach ( string str in subDirs )
                    dirs.Push(str);
            }

            return resfiles;
        }

        /// <summary>
        /// Enum enumerator
        /// prints enums
        /// </summary>
        /// <param name="enumType"></param>
        public void ee (Type enumType) {
            try {
                foreach ( var e in Enum.GetValues(enumType) ) {
                    Console.WriteLine((int)e + " -> " + e);
                }
            }
            catch {
                Debug.WriteLine("Ye booched something with enums");
            }
        }

        /// <summary>
        /// Modified ls2
        /// Finds empty folders under a root
        /// </summary>
        /// <param name="root"></param>
        public virtual List<String> list_empty_directories (string root) {
            // Data structure to hold names of subfolders to be
            // examined for files.
            Stack<string> dirs = new Stack<string>(20);
            var todelete = new List<String>();
            var resfiles = new List<System.IO.FileInfo>();


            if ( !System.IO.Directory.Exists(root) ) {
                throw new ArgumentException();
            }
            dirs.Push(root);

            while ( dirs.Count > 0 ) {
                string currentDir = dirs.Pop();
                string[] subDirs;
                try {
                    subDirs = System.IO.Directory.GetDirectories(currentDir);
                }
                // An UnauthorizedAccessException exception will be thrown if we do not have
                // discovery permission on a folder or file. It may or may not be acceptable 
                // to ignore the exception and continue enumerating the remaining files and 
                // folders. It is also possible (but unlikely) that a DirectoryNotFound exception 
                // will be raised. This will happen if currentDir has been deleted by
                // another application or thread after our call to Directory.Exists. The 
                // choice of which exceptions to catch depends entirely on the specific task 
                // you are intending to perform and also on how much you know with certainty 
                // about the systems on which this code will run.
                catch ( UnauthorizedAccessException e ) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                catch ( System.IO.DirectoryNotFoundException e ) {
                    Console.WriteLine(e.Message);
                    continue;
                }

                string[] files = null;
                try {
                    files = System.IO.Directory.GetFiles(currentDir);
                }

                catch ( UnauthorizedAccessException e ) {

                    Console.WriteLine(e.Message);
                    continue;
                }

                catch ( System.IO.DirectoryNotFoundException e ) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                // Perform the required action on each directory here.
                if ( files.Count() == 0 ) {
                    todelete.Add(currentDir);
                }

                // Push the subdirectories onto the stack for traversal.
                // This could also be done before handing the files.
                foreach ( string str in subDirs )
                    dirs.Push(str);
            }

            return todelete;
        }


        /// <summary>
        /// Runs rules loaded in memory returns a List-string- of empty directories
        /// </summary>
        public List<string> run_rules (List<rule> rules) {
            var emptydirs = new List<String>();

            // Using dir_collator: avoid running ls2 on repeat source directories encountered
            var DirDict = new Dictionary<string, List<FileInfo>>();

            foreach ( var rule in rules ) {
                IEnumerable<FileInfo> query;
                // Searches for file names using pattern as regex
                if ( rule.Regex ) {
                    Regex rgx = new Regex(rule.Pattern); // Caches pattern in regex engine without using static method Regex.IsMatch()
                    query = from file in dir_collator(rule.SourceDirectory, DirDict)
                                where rgx.IsMatch(file.Name)
                                select file;
                }
                // Searches for file names as *pattern*
                else {
                    query = from file in dir_collator(rule.SourceDirectory, DirDict)
                                where Compare.Contains(file.Name, rule.Pattern, StringComparison.OrdinalIgnoreCase)
                                select file;
                }
                Console.WriteLine("Items matched under root: " + query.Count());

                // I can't think of a better place to do this, for now.
                foreach ( var q in query ) {
                    Console.WriteLine("Operation: " + rule.Action + " on file " + q.FullName);
                    rule.process(q);

                    // Look for now possible empty directories
                    if ( rule.Action == rule.Actions.Move || rule.Action == rule.Actions.Delete ) {
                        if ( q.Directory.GetDirectories().Count() == 0 &&
                             q.Directory.GetFiles().Count() == 0 ) {
                            emptydirs.Add(q.DirectoryName);
                        }
                    }
                }
            }

            return emptydirs;
        }

        /// <summary>
        /// Takes a directory path and checks if it was already enumerated in cache
        /// </summary>
        /// <param name="SrcDir">The directory path to check in cache (DirDict)</param>
        /// <param name="DirDict">The directory cache</param>
        /// <returns>List of System.IO.FileInfo </returns>
        private List<System.IO.FileInfo> dir_collator(string SrcDir, Dictionary<string, List<FileInfo>> DirDict) {
            
            List<FileInfo> temp;

            // There's gotta be a way to better express this logic
            if ( DirDict.TryGetValue(SrcDir, out temp) ) {
                return temp;
            }
            // Don't incur Dict lookup on return by using temp
            temp = ls2(SrcDir);
            DirDict.Add(SrcDir, temp);

            return temp;
        }
    }
}

/// <summary>
/// This is the easiest way to redefining the Contains function for srt_helper.run_rules
/// </summary>
static class Compare {

    public static bool Contains (this string source, string toCheck, StringComparison comp) {
        return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
    }
}