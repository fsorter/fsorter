﻿using System;
using System.IO;
//using Microsoft.VisualBasic.FileIO; // it's still linked. I keep this line to be reminded of it

namespace sorter_cmd {

    /// <summary>
    /// Rule class specifiying rule objects for rulesets
    /// TODO: Consider if rule processing needs to be a separate class
    /// </summary>
    public class rule {
        private string pattern;
        private string sourcedirectory;
        private string destinationdirectory;
        private bool regex;
        private Actions action;
        public enum Actions : int {
            Copy = 0x01,
            Move = 0x02,
            Delete = 0x03,
            Nothing = 0x04,
        }

        public rule () {
            Pattern = null;
            SourceDirectory = null;
            DestinationDirectory = null;
            Regex = false;
            Action = Actions.Copy;
        }

        public rule (string pattern, string sourcedirectory, string destinationdirectory, bool regex, Actions action) {
            // Using the getter/setter methods, check for invalids paths 
            Pattern = pattern;
            SourceDirectory = sourcedirectory;
            DestinationDirectory = destinationdirectory;
            Regex = regex;
            Action = action;
        }

        public string Pattern
        {
            get { return pattern; }
            set { pattern = value; }
        }

        public string SourceDirectory
        {
            get { return sourcedirectory; }
            set { sourcedirectory = value; }
        }

        public string DestinationDirectory
        {
            get { return destinationdirectory; }
            set { destinationdirectory = value; }
        }

        public bool Regex
        {
            get { return regex; }
            set { regex = value; }
        }

        public Actions Action
        {
            get { return action; }
            set { action = value; }
        }

        private void copy (System.IO.FileInfo sourcefile) {
            System.IO.File.Copy(sourcefile.FullName, System.IO.Path.Combine(destinationdirectory, sourcefile.Name));
        }

        private void move (System.IO.FileInfo sourcefile) {
            System.IO.File.Move(sourcefile.FullName, System.IO.Path.Combine(destinationdirectory, sourcefile.Name));
        }

        private void delete (System.IO.FileInfo sourcefile) {
            System.IO.File.Delete(sourcefile.FullName);
        }

        /// <summary>
        /// TODO: Move this and the file ops functions out into a separate thread
        /// I don't know how yet, but it's time to rethink program execution
        /// Otherwise, I won't be able to freely modify file ops based on operating system 
        /// </summary>
        /// <param name="sourcefile"></param>
        /// <returns></returns>
        public bool process (System.IO.FileInfo sourcefile) {
            bool check = false;

            // Throw exception instead? Too expensive?
            if ( !System.IO.File.Exists(sourcefile.FullName) ) {
                Console.WriteLine("File: " + sourcefile.FullName + " doesn't exist");
                return false;
            }

            try {
                switch ( action ) {
                    case Actions.Copy:
                        this.copy(sourcefile);
                        // post transfer check
                        check = File.Exists(System.IO.Path.Combine(destinationdirectory, sourcefile.Name));
                        break;
                    case Actions.Move:
                        this.move(sourcefile);
                        // post transfer check
                        check = File.Exists(System.IO.Path.Combine(destinationdirectory, sourcefile.Name)) && !File.Exists(sourcefile.FullName);
                        break;
                    case Actions.Delete:
                        this.delete(sourcefile);
                        // post deletion check
                        check = !File.Exists(sourcefile.FullName);
                        break;
                    case Actions.Nothing:
                        break;
                    default:
                        break;
                }
            }
            catch ( Exception e ) when ( e is UnauthorizedAccessException ||
                    e is ArgumentNullException ||
                    e is ArgumentException ||
                    e is PathTooLongException ||
                    e is DirectoryNotFoundException ||
                    e is FileNotFoundException ||
                    e is IOException ||
                    e is NotSupportedException ) {
                Console.WriteLine(e.Message);
                return false;
            }

            return check;
        }

        public override string ToString () {
            return @"Pattern: " + pattern + "\n"
                        + "Src Dir: " + sourcedirectory + "\n"
                        + "Dst Dir: " + destinationdirectory + "\n"
                        + "Regex: " + regex + "\n"
                        + "Action: " + Enum.GetName(typeof(rule.Actions), action);
        }
    }
}
