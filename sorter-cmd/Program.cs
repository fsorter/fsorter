﻿using System;
using System.Collections.Generic;
using System.IO;


namespace sorter_cmd
{
    /// <summary>
    /// Program main entry point
    /// </summary>
    public class Runner
    {
        static srt_helper srth = new srt_helper();
        static string appdatalocation, ruleset_location = null;
        static List<rule> rules = new List<rule>();
        static Configuration config;
        static rule_storage storage;
        /// <summary>
        /// Start point of the application
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// 
        [STAThreadAttribute] // Necessary for the folder picker
        static int Main (string[] args) {

            //Run environment sanity and application initialization method first
            var run = new Runner();
            config = new Configuration(args);
            run.init_app();

            if ( config.isValid ) {
                if ( args.Length > 0 ) {
                    run.noninteractive_run(config, run);
                }
                else {
                    run.interactive_run();
                }
            }
            
            return 0;
        }

        private void interactive_run () {
            Console.WriteLine("Welcome to Sorter (v" + config.app_version + ")!");
            var run = new Runner();
            for (;;) {
                Console.WriteLine("Please select from the following: ");
                Console.WriteLine("1. Write new rule");
                Console.WriteLine("2. List loaded rules");
                Console.WriteLine("3. Save rules");
                Console.WriteLine("4. Load rules");
                Console.WriteLine("5. Delete rule");
                Console.WriteLine("6. Run rules");
                Console.WriteLine("7. Quit");

                int response;
                int.TryParse(Console.ReadLine(), out response);
                Console.Clear();
                switch ( response ) {
                    case 1:
                        if ( run.new_rule() )
                            Console.WriteLine("Rule written");
                        else
                            Console.WriteLine("Selection failure");
                        break;
                    case 2:
                        run.list_rules();
                        continue;
                    case 3:
                        storage.save_rules(ruleset_location, rules);
                        Console.WriteLine("Rules saved");
                        continue;
                    case 4:
                        rules = storage.load_rules(ruleset_location);
                        continue;
                    case 5:
                        run.delete_rule();
                        continue;
                    case 6:
                        run.delete_empty_directories_prompt(srth.run_rules(rules));
                        continue;
                    case 7:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Enter a number between 1 and 4");
                        break;
                }
                Console.Clear();
            }
        }

        private void noninteractive_run (Configuration config, Runner run) {

            // If user wants move-as-copy modify the ruleset before processing it by the file engine
            if ( config.moveascopy ) {
                ConvertAllMovetoCopy(rules);
            }

            // Technically, I don't need to check config.runrules but who knows what I want to support aside from -r in the future
            if ( config.runrules ) {
                if ( config.nodelete ) {
                    var nodelete_rules = rules.FindAll(FindNonDeleteRules);
                    run.delete_empty_directories_prompt(srth.run_rules(nodelete_rules));
                    return;
                }
                run.delete_empty_directories_prompt(srth.run_rules(rules));
            }
        }

        /// <summary>
        /// Lifted straight from http://stackoverflow.com/questions/16500080/how-to-create-appdata-folder-with-c-sharp
        /// Helps with appdata init
        /// Sets the appdatalocation setting
        /// TODO: Should be moved to a permenant configuration class
        /// </summary>
        /// <returns></returns>
        private void init_app () {

            // Which platform am I on
            int plat = (int)Environment.OSVersion.Platform;

            if ( plat == 4 || plat == 6 || plat == 128 ) {
                config.isLinux = true;
            }

            // The folder for the roaming current user 
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            // Combine the base folder with your specific folder....
            string specificFolder = Path.Combine(folder, @"fsorter");

            // Check if folder exists and if not, create it
            if ( !Directory.Exists(specificFolder) )
                Directory.CreateDirectory(specificFolder);

            appdatalocation = specificFolder;
            ruleset_location = Path.Combine(appdatalocation, @"fsorter.ruleset");

            // Assume that we're only dealing with XML

            init_xml();
        }

        /// <summary>
        /// Sets up XML as rule provider
        /// </summary>
        public void init_xml () {
            storage = new rule_storage_xml();
            rules = storage.load_rules(ruleset_location);
        }

        /// <summary>
        /// Deletes rule from memory
        /// </summary>
        public void delete_rule () {

            Console.WriteLine("Enter rule number to delete: ");
            this.list_rules();
            int response;
            int.TryParse(Console.ReadLine(), out response);

            rules.RemoveAt(response);

            Console.WriteLine("Rule deleted, please save current ruleset");

        }

        /// <summary>
        /// Prompt user to delete empty directories
        /// </summary>
        /// <param name="emptydirs"></param>
        private void delete_empty_directories_prompt (List<string> emptydirs) {

            if ( config.nocleanup ) {
                // Logging: info/debug
                Console.WriteLine("Skipping cleanup due to nocleanup set");
                return;
            }

            if ( emptydirs.Count > 0 ) {
                Console.WriteLine("The following directories are now empty: ");
                foreach ( var dir in emptydirs ) {
                    Console.WriteLine(dir);
                }

                string response = "n"; // initialized to "n" just in case
                if ( config.quietcleanup ) {
                    response = "y";
                }
                else {
                    Console.WriteLine("Delete (y/n)?");
                    response = Console.ReadLine();
                }

                if ( response.Equals("y", StringComparison.CurrentCultureIgnoreCase) ) {
                    foreach ( var dir in emptydirs ) {
                        System.IO.Directory.Delete(dir);
                    }
                    Console.WriteLine("Directories purged");
                    return;
                }
                Console.WriteLine("Directories kept");
            }
        }

        /// <summary>
        /// Lists rules in memory 
        /// </summary>
        public void list_rules()
        {
            var i = 0;
            foreach (var rule in rules)
            {
                Console.WriteLine("Rule #" + i);
                Console.WriteLine(rule);
                Console.WriteLine();
                i++;
            }
        }

        /// <summary>
        /// Writes a new rule to memory
        /// </summary>
        public bool new_rule()
        {
            Console.WriteLine("Regex (y/n)?");
            var response = Console.ReadLine();
            bool regex = false;
            if ( response.Equals("y", StringComparison.CurrentCultureIgnoreCase) )
                regex = true;

            Console.WriteLine("Enter pattern: ");
            var pattern = Console.ReadLine();

            string srcdir = null, destdir = null;
            // *nix folder picking
            if ( config.isLinux ) {
                // TODO Write an invalid character detector
                //char[] invalids = Path.GetInvalidPathChars();
                
                // Process from possible linux problems 

                Console.WriteLine("Specify source directory: ");
                srcdir = this.linux_trim(Console.ReadLine());
                
                Console.WriteLine("Specify destination directory: ");
                destdir = this.linux_trim(Console.ReadLine());
            }
            // Windows folder picking
            else {
                //Doc: Why don't you check paths better? CommonFileDialog does that for me.
                Console.WriteLine("Specify source directory: ");
                srcdir = srth.FolderPicker(false)[0];

                if (srcdir == "" ) {
                    Console.WriteLine("Incomplete selection");
                    return false;
                }

                Console.WriteLine("Specify destination directory: ");
                destdir = srth.FolderPicker(false)[0];
                if ( destdir == "" ) {
                    Console.WriteLine("Incomplete selection");
                    return false;
                }
            }

            Console.WriteLine("Specify action:");
            srth.ee(typeof(rule.Actions));
            response = Console.ReadLine();
            rule.Actions action;
            

            if ( !Enum.TryParse(response, out action) ) {
                Console.WriteLine("Something went wrong when converting response to enum");
                return false;
            }

            rules.Add(new rule(pattern, srcdir, destdir, regex, action));
            return true;
        }


        /// <summary>
        /// Takes a List<rule> and edits every move rule to a copy rule
        /// </summary>
        /// <param name="rules"></param>
        /// <returns>List<rule></returns>
        private void ConvertAllMovetoCopy (List<rule> rules) {

            var owaru = rules.FindAll(FindMoveRules);
            foreach (var ow in owaru ) {
                ow.Action = rule.Actions.Copy;
            }

        }


        /// <summary>
        /// Deals with dragging and dropping folders onto the shell in linux DEs
        /// Not guaranteed to work in all cases
        /// </summary>
        /// <param name="srcstr"></param>
        /// <returns></returns>
        private string linux_trim (string srcstr) {

            string newstr = null;
            srcstr = srcstr.Trim();
            if ( srcstr.StartsWith("'") && srcstr.EndsWith("'") ) {
                newstr = srcstr.Substring(1, srcstr.Length - 2);
            }

            return newstr;
        }


        private static bool FindRuleByAction(rule rule, rule.Actions action) {
            if ( rule.Action == action ) {
                return false;
            }
            else {
                return true;
            }
        }

        private static bool FindNonDeleteRules(rule rule) {

            if (rule.Action == rule.Actions.Delete ) {
                return false;
            }
            else {
                return true;
            }

        }

        private static bool FindMoveRules(rule rule) {
            if ( rule.Action == rule.Actions.Move ) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}