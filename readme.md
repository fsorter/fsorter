File Sorter

Command line application to sort files based on a rule set.
It's still in an early stage but the current features work well enough and are
representative of what the application should offer as a whole.

fsorter runs on Windows and mono.

Licensed under the MIT license.